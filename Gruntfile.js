module.exports = function(grunt) {
  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),

    
    concat: {
      express: {
        src: [
            'source/main.js',
            'source/modules/**/*.js'

        ],
        dest: 'deploy/server.js'
      },
      
      public: {
        src: [
          'source/public/**/*.js',
          '!source/public/bower_components/**/*.js'
        ],
        dest: 'deploy/public/app.js'
      }

    },

    ngAnnotate: {
      options: {
        singleQuotes: true,
        add: true
      },
      public: {
        files: {
          'deploy/public/app.annotated.js':['deploy/public/app.js']
        }
      }
    },
    
    uglify: {
      ////////////////////
      public: {
        options: {
          mangle: true,
          banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
          '<%= grunt.template.today("yyyy-mm-dd") %> */'
        },
        src: [
          'deploy/public/app.annotated.js'
        ],
        dest: 'deploy/public/app.min.js'
      },
      ///////////////////
      server: {
        options: {
          mangle: true,
          banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
          '<%= grunt.template.today("yyyy-mm-dd") %> */'
        },
        src: [
          'deploy/server.js'
        ],
        dest: 'deploy/server.min.js'
      }
    },

    jshint: {
      options: {
        reporter: require('jshint-stylish')
      },
      all: [
        'source/main.js',
        'source/public/**/*.js',
        'source/modules/**/*.js'
      ],
      node: ['deploy/server.js']
    },
    
    copy: {
      public: {
        expand: true,
        cwd: 'source/public',
        src: [
            '*.html',
            '*.css'
            
        ],
        dest: 'deploy/public'
      }
    }

  });

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-ng-annotate');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-copy');


  grunt.registerTask('default', [
    'concat',
    'ngAnnotate',
    'jshint',
    'uglify',
    'copy'  
  ]);


};


