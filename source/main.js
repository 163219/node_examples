var express = require("express");
var app = express();

var multer = require("multer");
var upload = multer({
    dest: __dirname + "/assets"
});
var handlebars = require("express-handlebars");
app.engine("handlebars", handlebars({ defaultLayout: "main"}));
app.set("view engine", "handlebars");


var bodyParser = require("body-parser");

app.use(bodyParser.urlencoded({extended: false}));

app.post("/upload-image", 
    upload.single("car_image"),
    function(req, res) {
        console.info("name: %s", req.body.name);
        console.info("email: %s", req.body.email);
        console.info("mobile: %s", req.body.mobile);
        console.info("----- image details ------");
        for (var i in req.file)
            console.info("req.file[%s] = %s", i, req.file[i]);
        res.render("registered", {
            name: req.body.name,
            email: req.body.email,
            mobile: req.body.mobile,
            carImage: req.file.filename,
            type: req.file.mimetype
        });
    });

app.use(express.static(__dirname + "/public"));
app.use("/assets", express.static(__dirname + "/assets"));
app.use("/bower_components", express.static(__dirname + "/bower_components"));

app.use(function(req, res, next) {
    res.status(404);
    res.type("text/plain");
    res.send("File not found");
});

app.set("port", process.argv[2] || process.env.APP_PORT || 3000);
app.listen(app.get("port"), function() {
    console.info("Application started on port %s", app.get("port"));
});
